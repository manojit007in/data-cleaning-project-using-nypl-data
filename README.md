# Data Cleaning Project using NYPL data

We will be working with the amazing collection of New York Public Library’s restaurant menu. The information is one of the largest available till data and over time has been exploited by various historians, chefs, novelists as well as food enthusiast for manifold purposes. The collection started with around 9000 menus back in April 2011 comprising primarily of topics photographed by volunteers over several years. It has grown at an amazing rate since then to turn into a collection of 45,000 menus dating from 1840 to 2012. A major contribution behind these amazing achievement goes to Library collection enthusiast Miss Frank E. Buttolph (1850-1924) who single-handedly contributed to over half of its current size.

It’s indeed an exciting opportunity to cleanse & exploit the various dimensions of such an immense data set which spans over 172 years.

The Project is divided into following phases:

Part 1.  Overview and Initial Assessment
Part 2.  Data Cleaning with OpenRefine
Part 3. Checking Integrity Constraint violations using SQLite3
Part 4. Creating the Workflow diagrams using YesWorkflow Tool

